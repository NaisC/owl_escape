
function gameover() {

    if ( !($('#game_gameover').length) )
    {
        $('#gameWindow').append('<div id="game_gameover_bg"></div>');
        $('#game_gameover_bg').append('<div id="game_gameover"></div>');
        $('#game_gameover').append('<a class="mainMenu" href="#">Main Menu</a>');
        $('#game_gameover').append('<a class="resetLevel" href="#">Reset Level</a>');
    }

    this.show = function ()
    {
        $('#game_gameover_bg').fadeIn();
    }

    this.hide = function(){
        $('#game_gameover_bg').fadeOut();
    }
}