[
    {"type":"stats","clonePar":2,"timePar":10},
    {"type":"wall","x":0,"y":680,"repeat-x":2},
    {"type":"player","x":150,"y": 550},
    {"type":"andGate","inputs":[
        {"type":"lever","x":250,"y": 641},
        {"type":"lever","x":450,"y": 641}
    ], "outputs": [
        {"type":"door", "x":700,"y":218}
    ]},
    {"type":"goal","x":890,"y":540}
]