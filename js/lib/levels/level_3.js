[
    {"type":"stats","clonePar":1,"timePar":15},
    {"type":"wall","x":0,"y":680,"repeat-x":2},
    {"type":"player","x":150,"y": 550},
    {"type":"lever","x":350,"y": 641,"outputs":[
        {"type":"door", "x":500,"y":218},
        {"type":"notGate", "outputs":[
            {"type":"door", "x":700,"y":218}
        ]}
    ]},
    {"type":"goal","x":890,"y":540}
]