[
    {"type":"stats","clonePar":1,"timePar":10},
    {"type":"wall","x":0,"y":680,"repeat-x":2},
    {"type":"player","x":150,"y": 550},
    {"type":"lever","x":350,"y": 641,"outputs":[
        {"type":"door", "x":700,"y":218}
    ]},
    {
        "type":"tooltip","x":150,"y":641,"width":50,"height":50,
        "text":"<span id='tooltip_buttons'></span>"
    },
    {
        "type":"tooltip","x":350,"y":641,"width":50,"height":50,
        "text":"<span id='tooltip_switch'></span>"
    },
    {
        "type":"tooltip","x":650,"y":641,"width":50,"height":50,
        "text":"<span id='tooltip_clones'></span><span id='tooltip_switch_clones'></span>"
    },
    {"type":"goal","x":890,"y":540}
]