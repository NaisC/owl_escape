function timer() {

    var _countDown;

    if (!($('#game_timer').length)) {
        $('#gameWindow').append('<div id="game_timer"></div>');
        $('#game_timer').append('<p><i class="fa fa-clock-o" aria-hidden="true"></i> <span class="time">0s</span></p>');
    }

    // Create / Reset the timer
    this.createCountDown = function (timeRemaining) {
        var startTime = Date.now();
        _countDown = function () {
            return Math.floor( (timeRemaining - ( Date.now() - startTime ) ) / 1000);
        }
    };

    this.getRemainingTime = function () {
        return _countDown();
    }

    this.updateRemainingTime = function() {
        $('#game_timer .time').text(_countDown());
    }

    this.show = function() {
        this.updateRemainingTime();

        $('#game_timer').fadeIn();
    }

    this.hide = function(){
        $('#game_timer').fadeOut();
    }
}